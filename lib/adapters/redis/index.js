try {
  var redis = require('redis');
} catch(err) {
  throw new Error("You must manually npm install redis to enable the Redis adapter");
}

module.exports = (function(){
  var Adapter = function(opts) {
    opts = opts || {};
    this.expires = opts.expires || 0;
    this.prefix = opts.prefix || 'cart:';
    this.connection = redis.createClient(opts.port || 6379, opts.host || '127.0.0.1', opts);

    if (opts.auth) {
      this.connection.auth(opts.auth);
    }

    if (opts.select) {
      this.connection.select(opts.select);
    }
  }

  Adapter.prototype.save = function(cart, callback) {
    this.connection.set(this.prefix + cart.token, JSON.stringify(cart.items), function(err, res){
      if (err) return callback(err, null);

      if (this.expires > 0) {
        this.connection.expire(this.prefix + cart.token, this.expires, function(err, _res){
          if (err) return callback(err,  null);
          callback(null, cart);
        });
      } else {
        callback(null, cart);
      }
    }.bind(this));
  }

  Adapter.prototype.delete = function(token, callback) {
    this.connection.del(this.prefix + token, callback);
  }

  Adapter.prototype.find = function(token, callback) {
    this.connection.exists(this.prefix + token, function(err, exists){
      if (!exists) return callback(null, null);

      this.connection.get(this.prefix + token, function(err, res){
        callback(err, {token: token, items: JSON.parse(res)});
      });
    }.bind(this));
  }

  return Adapter;
})();