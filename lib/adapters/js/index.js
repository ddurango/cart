module.exports = (function(){
  var Adapter = function(opts) {
    this.bin = {};
  }

  Adapter.prototype.save = function(cart, callback) {
    this.bin[cart.token] = cart.items;
    callback(null, cart);
  }

  Adapter.prototype.delete = function(token, callback) {
    var binExists = !!this.bin[token];

    this.bin = this.bin.filter(function(bin){
      return bin.token !== token;
    });

    callback(null, binExists);
  }

  Adapter.prototype.find = function(token, callback) {
    callback(null, (this.bin[token] !== undefined ? this.bin[token] : null));
  }

  return Adapter;
})();