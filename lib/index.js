var uuid = require('node-uuid');

module.exports = (function(){
  var Increment = function(number) {
    if (!(this instanceof Increment))
      return new Increment(number);

    this.number = number;
  }

  Increment.prototype = {
    toString: function() {
      return parseFloat(this.number);
    }
  }

  var Cart = function(bin, token) {
    this.bin = bin;
    this.opts = bin.opts || {};
    this.token = token || uuid.v4();
    this.items = [];

    this.opts.attributes = this.opts.attributes || {};
    this.opts.attributes.subtotal = this.opts.attributes.subtotal || {field: 'subtotal', defaultValue: 0};
    this.opts.attributes.qty = this.opts.attributes.qty || {field: 'qty', defaultValue: 1};
  }

  Cart.prototype.add = function(items, callback) {
    var items = (Array.isArray(items) ? items : [items])
      , self = this;

    items.forEach(function(item){
      self.bin._triggers.add.forEach(function(fct){
        fct(item, self, function(err, props){
          if (err) return callback(err, null);
          self = props.cart;
          item = props.item;
        });
      });

      // Add our default/"required fields" for each item
      for (var key in self.opts.attributes) {
        if (!item[self.opts.attributes[key].field || self.opts.attributes[key]]) {
          item[self.opts.attributes[key].field || self.opts.attributes[key]] = (self.opts.attributes[key].defaultValue !== undefined ? self.opts.attributes[key].defaultValue : self.opts.attributes[key]);
        }
      }

      self.items.push(item);
    });

    self.bin.save(self, callback);
  }

  Cart.prototype.incr = function(number) {
    return new Increment(number);
  }

  Cart.prototype.update = function(items, qty, callback) {
    var self = this
      , remove = []
      , field = self.opts.attributes.qty.field || self.opts.attributes.qty;

    this.search(items, function(err, indexes){
      indexes.forEach(function(idx){
        if (qty instanceof Increment || qty < 0) {
          self.items[idx][field] += qty;
        } else {
          self.items[idx][field] = qty;
        }

        if (self.items[idx][field] < 1) {
          remove.push(idx);
        }
      });

      self.items = self.items.filter(function(e, i) {
        return remove.indexOf(i) < 0;
      });

      self.bin.save(self, callback);
    });
  }

  Cart.prototype.save = function(callback) {
    this.bin.save(this.token, callback);
  }

  Cart.prototype.delete = function(callback) {
    this.bin.delete(this.token, callback);
  }

  Cart.prototype.remove = function(items, opts, callback) {
    var self = this;

    if (typeof opts === "function") {
      callback = opts;
      opts = {};
    }

    self.search(items, function(err, indexes){
      self.items = self.items.filter(function(e, i) {
        return indexes.indexOf(i) < 0;
      });

      self.bin.save(self, callback);
    });
  }

  Cart.prototype.search = function(items, callback) {
    var items = (Array.isArray(items) ? items : [items])
      , indexes = []
      , self = this;

    items.forEach(function(item){
      var keyLength = Object.keys(item).length;

      self.items.forEach(function(cartItem, index){
        var ticker = 0;

        for (var key in item) {
          if (cartItem[key] !== undefined && cartItem[key] === item[key]) {
            ticker++;
          }
        }

        if (ticker === keyLength) {
          indexes.push(index);
        }
      });
    });

    callback(null, indexes);
  }

  return Cart;
})();