//var redis = require('redis');
var events = require('events')
  , util = require('util');

var Cart = require('./lib');

module.exports = (function(){
  var CartBin = function(opts) {
    this.opts = opts || {};
    this._triggers = {add: [], remove: []};
    this.dialect = this.opts.adapter || ['js', {}];

    try {
      var adapter = require('./lib/adapters/' + this.dialect[0]);
    } catch(err) {
      throw new Error(this.dialect[0] + " is not a valid adapter.");
    }

    this.adapter = new adapter(this.dialect[1]);
  }

  CartBin.prototype.find = function(token, callback) {
    this.adapter.find(token, function(err, c){
      if (err || c === null || c.items === null)
        return callback(err, null);

      var cart = new Cart(this, token);
      cart.items = c.items;
      callback(null, cart);
    }.bind(this));
  }

  CartBin.prototype.findOrCreate = function(token, callback) {
    this.find(token, function(err, res){
      if (res !== null) return callback(err, res);

      if (err) return callback(err, null);
      this.create(token, callback);
    }.bind(this));
  }

  CartBin.prototype.save = function(cart, callback) {
    this.adapter.save(cart, callback);
  }

  CartBin.prototype.delete = function(token, callback) {
    this.adapter.delete(token, callback);
  }

  CartBin.prototype.create = function(token, callback) {
    if (typeof token === "function") {
      callback = token;
      token = undefined;
    }

    var cart = new Cart(this, token);
    this.save(cart, function(err, res){
      callback(err, cart);
    });
  }

  CartBin.prototype.triggers = function(type, fct) {
    this._triggers[type] = this._triggers[type] || [];
    this._triggers[type].push(fct);
  }

  return CartBin;
})();