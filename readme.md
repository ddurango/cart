# Examples

  :::javascript

    var CartBin = require('./index')
      , cartBin = new CartBin({
        adapter: ['redis']
      });

    cartBin.triggers('add', function(item, cart, callback){
      if (item.item && item.item === "hello") {
        item.item = 'test2222';
      }

      callback(null, {item: item, cart: cart});
    });

    cartBin.find('b99b4dea-9be8-47f7-94ac-51e173d3f86d', function(err, cart){
      console.log(cart);
    });

    cartBin.find('ada3449d-4d23-4115-8fdd-7c8a1293f00a', function(err, cart){
      console.log('c', cart);
    });

    cartBin.findOrCreate('12343', function(err, cart){
      if (err) throw new Error(err);
      cart.delete(function(err, res){
        cartBin.find('12343', function(err, cart){
          console.log('cart?', cart);
        });
      })
    })

    cartBin.create(function(err, cart){
      cart.add([{item: 'hello'}, {item: 'test'}], function(err, cart){
        cart.remove([{qty: 0, item: 'test2222'}], function(err, cart){
          cart.update({item: 'test'}, cart.incr(30), function(err, cart){
            console.log('cart', cart);
          });
        });
      });
    });