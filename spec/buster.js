var config = module.exports;

config["node tests"] = {
    env: "node",
    rootPath: "../",
    tests: [
        "spec/*-test.js"
    ]
};