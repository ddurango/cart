var buster = require('buster')
  , CartBin = require('../index')
  , cartBin = new CartBin({
    adapter: ['redis']
  });

buster.spec.expose();

describe("cartBin()", function(){
  describe("#create", function(){
    it("it should create a cart with a random token", function(done){
      cartBin.create(function(err, cart){
        expect(err).toBeNull();
        expect(cart).toBeDefined();
        expect(cart).toBeObject();
        expect(cart.token).toBeDefined();
        expect(cart.token).toBeString();
        expect(cart.items).toBeDefined();
        expect(cart.items).toBeArray();
        expect(cart.items.length).toEqual(0);
        done();
      });
    });
  });

  describe("#find", function(){
    before(function(done){
      cartBin.create('123', function(err, cart){
        this.cart = cart;
        cartBin.delete('12345', function(err, res){
          done();
        });
      }.bind(this));
    });

    it("should be able to find the cart", function(done){
      cartBin.find('123', function(err, cart){
        expect(err).toBeNull();
        expect(cart).toBeDefined();
        expect(cart.token).toBeDefined();
        expect(cart.token).toBeString();
        expect(cart.token).toEqual('123');
        expect(cart.items).toBeDefined();
        expect(cart.items).toBeArray();
        expect(cart.items.length).toEqual(0);
        done();
      });
    });

    it("should have trouble finding a non-existant cart", function(done){
      cartBin.find('404', function(err, cart){
        expect(err).toBeNull();
        expect(cart).toBeNull();
        done();
      });
    });

    it("should find or create a cart", function(done){
      cartBin.find('12345', function(err, cart){
        expect(err).toBeNull();
        expect(cart).toBeNull();

        cartBin.findOrCreate('12345', function(err, cart){
          expect(err).toBeNull();
          expect(cart).toBeDefined();
          expect(cart).toBeObject();
          expect(cart.token).toBeDefined();
          expect(cart.token).toBeString();
          expect(cart.token).toEqual('12345');
          expect(cart.items).toBeDefined();
          expect(cart.items).toBeArray();
          expect(cart.items.length).toEqual(0);
          done();
        });
      });
    });
  });
});
