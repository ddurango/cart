var buster = require('buster')
  , CartBin = require(__dirname + '/../index')
  , cartBin = new CartBin({
    adapter: ['redis']
  });

buster.spec.expose();

describe("Cart()", function(){
  before(function(done){
    cartBin.findOrCreate('1234', function(err, cart){
      this.cart = cart;
      this.cart.items = [];
      done();
    }.bind(this))
  });

  describe("#update", function() {
    it("should be able to update an item's quantity", function(done){
      this.cart.add([{id: 1, qty: 50}, {id: 5, qty: 2}], function(err, cart){
        cart.update({id: 1}, cart.incr(30), function(err, cart){
          expect(err).toBeNull();
          expect(cart).toBeDefined();
          expect(cart).toBeObject();
          expect(cart.token).toBeDefined();
          expect(cart.token).toBeString();
          expect(cart.token).toEqual('1234');
          expect(cart.items).toBeDefined();
          expect(cart.items).toBeArray();
          expect(cart.items.length).toEqual(2);
          expect(cart.items[0]).toBeDefined();
          expect(cart.items[0]).toBeObject();
          expect(cart.items[0].id).toBeDefined();
          expect(cart.items[0].id).toBeNumber();
          expect(cart.items[0].id).toEqual(1);
          expect(cart.items[0].qty).toBeDefined();
          expect(cart.items[0].qty).toBeNumber();
          expect(cart.items[0].qty).toEqual(80);
          expect(cart.items[0].subtotal).toBeDefined();
          expect(cart.items[0].subtotal).toBeNumber();
          // expect(cart.items[0].subtotal).toEqual(); #TODO
          expect(cart.items[1].id).toBeDefined();
          expect(cart.items[1].id).toBeNumber();
          expect(cart.items[1].id).toEqual(5);
          expect(cart.items[1].qty).toBeDefined();
          expect(cart.items[1].qty).toBeNumber();
          expect(cart.items[1].qty).toEqual(2);
          expect(cart.items[1].subtotal).toBeDefined();
          expect(cart.items[1].subtotal).toBeNumber();
          // expect(cart.items[1].subtotal).toEqual(); #TODO
          done();
        });
      });
    });

    it("should be able to remove an item with quantity < 1", function(done) {
      this.cart.add([{id: 1, qty: 50}, {id: 5, qty: 2}], function(err, cart){
        cart.update({id: 1}, -50, function(err, cart){
          cart.update({id: 5}, -1, function(err, cart){
            expect(err).toBeNull();
            expect(cart).toBeDefined();
            expect(cart).toBeObject();
            expect(cart.token).toBeDefined();
            expect(cart.token).toBeString();
            expect(cart.token).toEqual('1234');
            expect(cart.items).toBeDefined();
            expect(cart.items).toBeArray();
            expect(cart.items.length).toEqual(1);
            expect(cart.items[0].id).toBeDefined();
            expect(cart.items[0].id).toBeNumber();
            expect(cart.items[0].id).toEqual(5);
            expect(cart.items[0].qty).toBeDefined();
            expect(cart.items[0].qty).toBeNumber();
            expect(cart.items[0].qty).toEqual(1);
            expect(cart.items[0].subtotal).toBeDefined();
            expect(cart.items[0].subtotal).toBeNumber();
            // expect(cart.items[0].subtotal).toEqual(); #TODO
            done();
          });
        });
      });
    });

    it("should change an item's quantity directly", function(done){
      this.cart.add([{id: 1, qty: 50}, {id: 5, qty: 2}], function(err, cart){
        cart.update({id: 1}, 20, function(err, cart){
          expect(err).toBeNull();
          expect(cart).toBeDefined();
          expect(cart).toBeObject();
          expect(cart.token).toBeDefined();
          expect(cart.token).toBeString();
          expect(cart.token).toEqual('1234');
          expect(cart.items).toBeDefined();
          expect(cart.items).toBeArray();
          expect(cart.items.length).toEqual(2);
          expect(cart.items[0]).toBeDefined();
          expect(cart.items[0]).toBeObject();
          expect(cart.items[0].id).toBeDefined();
          expect(cart.items[0].id).toBeNumber();
          expect(cart.items[0].id).toEqual(1);
          expect(cart.items[0].qty).toBeDefined();
          expect(cart.items[0].qty).toBeNumber();
          expect(cart.items[0].qty).toEqual(20);
          expect(cart.items[0].subtotal).toBeDefined();
          expect(cart.items[0].subtotal).toBeNumber();
          // expect(cart.items[0].subtotal).toEqual(); #TODO
          expect(cart.items[1].id).toBeDefined();
          expect(cart.items[1].id).toBeNumber();
          expect(cart.items[1].id).toEqual(5);
          expect(cart.items[1].qty).toBeDefined();
          expect(cart.items[1].qty).toBeNumber();
          expect(cart.items[1].qty).toEqual(2);
          expect(cart.items[1].subtotal).toBeDefined();
          expect(cart.items[1].subtotal).toBeNumber();
          // expect(cart.items[1].subtotal).toEqual(); #TODO
          done();
        });
      });
    });
  });

  describe("#remove", function(){
    it("should be able to remove an item", function(done){
      this.cart.add([{id: 4, type: 'product', sku: 'hello-123'}, {id: 5, sku: '123'}, {id: 6, sku: 'hello-123'}, {id: 10, sku: 'hello-123', type: 'product'}], function(err, cart){
        cart.remove({sku: 'hello-123', type: 'product'}, function(err, cart){
          expect(err).toBeNull();
          expect(cart).toBeDefined();
          expect(cart).toBeObject();
          expect(cart.token).toBeDefined();
          expect(cart.token).toBeString();
          expect(cart.token).toEqual('1234');
          expect(cart.items).toBeDefined();
          expect(cart.items).toBeArray();
          expect(cart.items.length).toEqual(2);
          expect(cart.items[0]).toBeDefined();
          expect(cart.items[0]).toBeObject();
          expect(cart.items[0].id).toBeDefined();
          expect(cart.items[0].id).toBeNumber();
          expect(cart.items[0].id).toEqual(5);
          expect(cart.items[0].sku).toBeDefined();
          expect(cart.items[0].sku).toBeString();
          expect(cart.items[0].sku).toEqual('123');
          expect(cart.items[0].qty).toBeDefined();
          expect(cart.items[0].qty).toBeNumber();
          expect(cart.items[0].qty).toEqual(1);
          expect(cart.items[0].subtotal).toBeDefined();
          expect(cart.items[0].subtotal).toBeNumber();
          // expect(cart.items[0].subtotal).toEqual(); #TODO
          expect(cart.items[1].id).toBeDefined();
          expect(cart.items[1].id).toBeNumber();
          expect(cart.items[1].id).toEqual(6);
          expect(cart.items[1].sku).toBeDefined();
          expect(cart.items[1].sku).toBeString();
          expect(cart.items[1].sku).toEqual('hello-123');
          expect(cart.items[1].qty).toBeDefined();
          expect(cart.items[1].qty).toBeNumber();
          expect(cart.items[1].qty).toEqual(1);
          expect(cart.items[1].subtotal).toBeDefined();
          expect(cart.items[1].subtotal).toBeNumber();
          // expect(cart.items[1].subtotal).toEqual(); #TODO
          done();
        });
      });
    });
  });

  describe("#add", function(){
    it("should be able to add one item with default values", function(done){
      this.cart.add({id: 4, type: 'product'}, function(err, cart){
        expect(err).toBeNull();
        expect(cart).toBeDefined();
        expect(cart).toBeObject();
        expect(cart.token).toBeDefined();
        expect(cart.token).toBeString();
        expect(cart.token).toEqual('1234');
        expect(cart.items).toBeDefined();
        expect(cart.items).toBeArray();
        expect(cart.items.length).toEqual(1);
        expect(cart.items[0]).toBeDefined();
        expect(cart.items[0]).toBeObject();
        expect(cart.items[0].id).toBeDefined();
        expect(cart.items[0].id).toBeNumber();
        expect(cart.items[0].id).toEqual(4);
        expect(cart.items[0].type).toBeDefined();
        expect(cart.items[0].type).toBeString();
        expect(cart.items[0].type).toEqual('product');
        expect(cart.items[0].subtotal).toBeDefined();
        expect(cart.items[0].subtotal).toBeNumber();
        // expect(cart.items[0].subtotal).toEqual(); #TODO
        expect(cart.items[0].qty).toBeDefined();
        expect(cart.items[0].qty).toBeNumber();
        expect(cart.items[0].qty).toEqual(1);
        done();
      });
    });

    it("should be able to able to add more than one item.", function(done){
      this.cart.add([{id: 4, type: 'product'},{id: 10, type: 'product', qty: 5}], function(err, cart){
        expect(err).toBeNull();
        expect(cart).toBeDefined();
        expect(cart).toBeObject();
        expect(cart.token).toBeDefined();
        expect(cart.token).toBeString();
        expect(cart.token).toEqual('1234');
        expect(cart.items).toBeDefined();
        expect(cart.items).toBeArray();
        expect(cart.items.length).toEqual(2);
        expect(cart.items[0]).toBeDefined();
        expect(cart.items[0]).toBeObject();
        expect(cart.items[0].id).toBeDefined();
        expect(cart.items[0].id).toBeNumber();
        expect(cart.items[0].id).toEqual(4);
        expect(cart.items[0].type).toBeDefined();
        expect(cart.items[0].type).toBeString();
        expect(cart.items[0].type).toEqual('product');
        expect(cart.items[0].subtotal).toBeDefined();
        expect(cart.items[0].subtotal).toBeNumber();
        // expect(cart.items[0].subtotal).toEqual(); #TODO
        expect(cart.items[0].qty).toBeDefined();
        expect(cart.items[0].qty).toBeNumber();
        expect(cart.items[0].qty).toEqual(1);
        expect(cart.items[0]).toBeDefined();
        expect(cart.items[0]).toBeObject();
        expect(cart.items[1].id).toBeDefined();
        expect(cart.items[1].id).toBeNumber();
        expect(cart.items[1].id).toEqual(10);
        expect(cart.items[1].type).toBeDefined();
        expect(cart.items[1].type).toBeString();
        expect(cart.items[1].type).toEqual('product');
        expect(cart.items[1].subtotal).toBeDefined();
        expect(cart.items[1].subtotal).toBeNumber();
        // expect(cart.items[1].subtotal).toEqual(); #TODO
        expect(cart.items[1].qty).toBeDefined();
        expect(cart.items[1].qty).toBeNumber();
        expect(cart.items[1].qty).toEqual(5);
        done();
      });
    });
  });
});